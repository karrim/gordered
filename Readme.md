# Gordered #
Moves files following a set of rules. 
## Installation ##
```
go get bitbucket.org/karrim/gordered
```
## Building ##
### Requirements ###
* Golang SDK

### Build ###
```
go build bitbucket.org/karrim/gordered
```
## Configuration ##
Rules are defined in json format:
```
{
  "/path/to/directory": {
    "regexp": "relative/or/absolute/file/path"
  },
  "/path/to/directory": {
    "regexp": "relative/or/absolute/file/path"
  }
}
```
## Running ##
```
gordered -config path/to/config/file
```
Default flag values:

* config:
    * linux: ```~/.config/gordered/rules.json```
    * windows: ```C:\Users\\<UserName>\AppData\Local\gordered\rules.json```