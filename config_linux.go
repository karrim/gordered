// +build linux
package gordered

import "path/filepath"

func ConfigPath(home string) string {
	return filepath.Join(home, ".config/gordered/rules.json")
}
