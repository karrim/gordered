// +build windows
package gordered

import "path/filepath"

func ConfigPath(home string) string {
	return filepath.Join(home, "AppData\\Local\\gordered\\rules.json")
}
