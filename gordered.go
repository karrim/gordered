package gordered

import (
	"encoding/json"
	"errors"
	"github.com/fsnotify/fsnotify"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

type Rule struct {
	Regex  *regexp.Regexp
	Target string
}

func ParseRules(bs []byte) (map[string][]*Rule, error) {
	var v interface{}
	err := json.Unmarshal(bs, &v)
	if nil != err {
		return nil, err
	}
	values, ok := v.(map[string]interface{})
	if !ok {
		return nil, errors.New("Invalid rules format.")
	}
	rules := make(map[string][]*Rule)
	for dirname, cfg := range values {
		rs, ok := cfg.(map[string]interface{})
		if !ok {
			return nil, errors.New("Invalid rules format.")
		}
		rules[dirname] = make([]*Rule, len(rs))
		i := 0
		for match, t := range rs {
			target, ok := t.(string)
			if !ok {
				return nil, errors.New("Invalid rules format.")
			}
			if !filepath.IsAbs(target) {
				target = filepath.Join(dirname, target)
			}
			rules[dirname][i] = &Rule{
				Regex:  regexp.MustCompile(match),
				Target: target,
			}
			i++
		}
	}
	return rules, nil
}

func MoveAll(config map[string][]*Rule) error {
	for dirname, rules := range config {
		info, err := os.Stat(dirname)
		if nil != err {
			return err
		}
		if !info.IsDir() {
			return errors.New(dirname + " is not a directory.")
		}
		filepath.Walk(dirname, func(path string, info os.FileInfo, err error) error {
			if nil != err {
				return err
			}
			if info.IsDir() {
				return nil
			}
			fname := filepath.Base(path)
			for _, rule := range rules {
				if strings.Contains(path, rule.Target) {
					continue
				}
				if rule.Regex.MatchString(fname) {
					err = os.Rename(path, filepath.Join(rule.Target, fname))
					if nil != err {
						return err
					}
					break
				}
			}
			return nil
		})
	}
	return nil
}

func SetupWatcher(config map[string][]*Rule) (chan bool, error) {
	watcher, err := fsnotify.NewWatcher()
	if nil != err {
		return nil, err
	}
	done := make(chan bool)
	go func() {
		defer watcher.Close()
		for {
			select {
			case event := <-watcher.Events:
				switch event.Op {
				case fsnotify.Create:
				case fsnotify.Rename:
				case fsnotify.Write:
					fpath := event.Name
					fname := filepath.Base(fpath)
					_, err := os.Stat(fpath)
					if nil != err {
						if os.IsNotExist(err) {
							break
						}
						panic(err)
					}
					for dirname, rules := range config {
						if strings.Contains(fpath, dirname) {
							for _, rule := range rules {
								if rule.Regex.MatchString(fname) {
									newpath := filepath.Join(rule.Target, fname)
									err = os.Rename(fpath, newpath)
									if nil != err {
										panic(err)
									}
									break
								}
							}
							break
						}
					}
					break
				}
			case err := <-watcher.Errors:
				panic(err)
			}
		}
	}()
	for dirname, _ := range config {
		info, err := os.Stat(dirname)
		if nil != err {
			return nil, err
		}
		if !info.IsDir() {
			return nil, errors.New(dirname + " is not a directory.")
		}
		watcher.Add(dirname)
	}
	return done, nil
}
