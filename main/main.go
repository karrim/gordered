package main

import (
	"bitbucket.org/karrim/gordered"
	"flag"
	"github.com/mitchellh/go-homedir"
	"io/ioutil"
	"path/filepath"
)

func main() {
	home, err := homedir.Dir()
	if nil != err {
		panic(err)
	}
	configPath := flag.String("config", filepath.Join(gordered.ConfigPath(home)), "path of config file")
	flag.Parse()

	bs, err := ioutil.ReadFile(*configPath)
	if nil != err {
		panic(err)
	}
	config, err := gordered.ParseRules(bs)
	if nil != err {
		panic(err)
	}
	err = gordered.MoveAll(config)
	if nil != err {
		panic(err)
	}
	done, err := gordered.SetupWatcher(config)
	<-done
}
